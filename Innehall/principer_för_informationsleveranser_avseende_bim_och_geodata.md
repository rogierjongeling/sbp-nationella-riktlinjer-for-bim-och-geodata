Principer för informationsleveranser avseende BIM och geodata
=============================================================

 

Livscykeln och processer
------------------------

Livscykelhantering av information rörande objekt i den byggde miljön består av
ett antal livscykelsteg.

Varje livscykelsteg består av ett antal processer.

Olika indelningsgrunder för livscykelsteg och processer kan förekomma beroende
på typ av verksamhet, projekt och/eller inblandade parter.

 

Aktiviteter och metoder
-----------------------

Inom varje process genomförs ett antal olika aktiviteter där metoder används som
bland annat behöver och resulterar i informationsleveranser innehållande
informationsmängder.

 

Informationsmängder och modeller
--------------------------------

Informationsmängder kan vara dokument- och/eller modellorienterade. Beroende på
typ av verksamhet, projekt, byggnadsverk eller systemstöd kan olika typer av
modeller, bestående av objekt, identifieras så som geometri-, anläggnings- eller
byggnadsinformationsmodeller.

Nivån eller mognadsgraden gällande hantering av informationsmängder kan variera.
Den kan vara baserat på dokument (nivå 1), modell (nivå 2), objekt (nivå 3)
eller en kombination av dessa.

 

Lagring och utbyte
------------------

Informationsmängder rörande objekt utväxlas och lagras som en avgränsad
datamängd med hjälp av olika resurser så som tekniska hjälpmedel i form av
CAD-verktyg, GIS-verktyg och drift- och förvaltningssystem.

Utväxling och lagring kan ske med hjälp av filer eller databaskopplingar som kan
vara tenikneutrala så som CityGML eller IFC.

 

Struktureringsmetoder
---------------------

För att välja ut, sortera och förändra informationsmängder används en eller
flera struktureringsmetoder så som identifiering och klassificering.

I Sverige är CoClass lämplig som systematik för strukturering av
informationsmängder ur ett livscykelperspektiv.

 

Leveransspecifikation och leveransmeddelande
--------------------------------------------

För varje informationsleverans används en leveransspecifikation och ett
leveransmeddelande.

Från leveransspecifikationen framgår det alla egenskaper som har betydelse för
mottagarens förmåga att ta emot, lagra och vidareanvända informationsmängderna,
bland annat informationsmängdens omfattning och dess bestämningsgrad.

 

Bestämningsgrad och LOD
-----------------------

Bestämningsgraden sker genom märkning av en informationsmängd och utgörs av
kombinationen av livscykelsteg, informationsnivå och en eller flera aspekt(er).
LOD (Level of Detail) används för att beskriva den geometriska detaljeringen av
objekt i definitionen av informationsnivå.
